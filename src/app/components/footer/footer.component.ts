import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  date: Date = new Date();
  meses:string[]=["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Dciembre"]
  dia:number
  mes:string
  anio:number

  constructor() {
    this.dia = this.date.getDate();
    this.mes = this.meses[this.date.getMonth()];
    this.anio = this.date.getFullYear();
   }

  ngOnInit(): void {
  }

}
